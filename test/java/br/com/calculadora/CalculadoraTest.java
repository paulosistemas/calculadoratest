package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    private Calculadora calculadora;

    @BeforeEach
    public void SetupXablau(){
        calculadora = new Calculadora();
    }

    @Test
    public void TestaASomaDeDoisNumerosInteiros(){
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3,resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.699999999999999, resultado);

    }

    @Test
    public void testaMultiplicacaoInteiros(){
        double resultado = calculadora.multiplica(4, 3);

        Assertions.assertEquals(12, resultado);
    }

    @Test
    public void testaMultiplicacaoNegativos(){
        double resultado = calculadora.multiplica(-4, 3);

        Assertions.assertEquals(-12, resultado);
    }

    @Test
    public void testaMultiplicacaoFlutuantes(){
        double resultado = calculadora.multiplica(-4.3, 3.2);

        Assertions.assertEquals(-13.76, resultado);
    }

    @Test
    public void testaDivisaoFlutuantes(){
        double resultado = calculadora.divisao(-4.3, 3.2);

        Assertions.assertEquals( -1.3437499999999998, resultado);
    }

    @Test
    public void testaDivisaoInteiros(){
        int resultado = calculadora.divisao(4, 3);

        Assertions.assertEquals( 1, resultado);
    }

}
