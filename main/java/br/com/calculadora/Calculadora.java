package br.com.calculadora;

import java.math.BigDecimal;

public class Calculadora {

    public Calculadora() {

    }

    public int soma(int primeiroNumero, int segundoNumero) {
//        if (primeiroNumero<0 || segundoNumero<0){
//            throw new Exception;
//        }
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero) {
        Double resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public int multiplica(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public double multiplica(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public double divisao(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public int divisao(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }
}
